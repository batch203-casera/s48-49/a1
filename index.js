// GET ALL DATA 

let fetchMovies = () => {
    return movies = fetch("https://sheltered-hamlet-48007.herokuapp.com/movies")
        .then(response => response.json())
        .then(data => showPosts(data));
}

function showPosts(movies) {
    let movieEntries = "";
    movies.forEach(e => {
        let entry = `
            <div id="movie-${e._id}">
                <h3 id="movie-title-${e._id}">
                ${e.title}
                </h3>

                <p id="movie-desc-${e._id}">
                ${e.description}
                </p>

                <button onclick="editMovie('${e._id}')">Edit</button>
                <button onclick="deleteMovie('${e._id}')">Delete</button>
            </div>
        `;
        movieEntries += entry;
    });
    document.getElementById("div-movie-entries").innerHTML = movieEntries;
}

fetchMovies();


document.querySelector("#form-add-movie")
    .addEventListener('submit', e => {
        e.preventDefault();
        let title = document.querySelector("#txt-title").value;
        let desc = document.querySelector("#txt-desc").value;

        fetch("https://sheltered-hamlet-48007.herokuapp.com/movies",
            {
                method: "POST",
                body: JSON.stringify({
                    "title": title,
                    "description": desc
                }),
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then(response => response.json())
            .then(data => { fetchMovies(); alert('Success') })
            .catch(err => console.log(err));


    });

document.querySelector("#form-edit-movie")
    .addEventListener('submit', e => {
        e.preventDefault();
        let id = document.querySelector("#txt-edit-id").value;
        let title = document.querySelector("#txt-edit-title").value;
        let desc = document.querySelector("#txt-desc-edit").value;

        fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`,
            {
                method: "PUT",
                body: JSON.stringify({
                    "title": title,
                    "description": desc
                }),
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then(response => response.json())
            .then(data => { fetchMovies(); alert('Success'); document.querySelector("#btn-submit-update").setAttribute('disabled', true); document.querySelector("#txt-edit-id").value = null; document.querySelector("#txt-edit-title").value = null; document.querySelector("#txt-desc-edit").value = null; })
            .catch(err => console.log(err));

    });


function deleteMovie(id) {
    fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`,
        {
            method: "DELETE",
        })
        .then(response => response.json())
        .then(data => { fetchMovies(); alert('Successfully Deleted') })
        .catch(err => console.log(err));
}


function editMovie(id) {
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = document.getElementById(`movie-title-${id}`).innerText;
    document.querySelector("#txt-desc-edit").value = document.getElementById(`movie-desc-${id}`).innerText;
    document.querySelector("#btn-submit-update").removeAttribute('disabled');
}